package br.com.php.connSecured.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

public class Test {

	public void myHostVerifier(){
		HostnameVerifier hv = new HostnameVerifier() {
			
			@Override
			public boolean verify(String hostname, SSLSession session) {
				// TODO Auto-generated method stub
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
	}
	
	/**
	 * Add VM arguments:
	 * -Djavax.net.ssl.trustStore=F:/MyCert.cert
	 * -Djavax.net.ssl.trustStorePass=password
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Test test = new Test();
			
			/**
				Call this when host is not verified, for localhost, dev enviroment only
			*/
			test.myHostVerifier();
			
			/**
				Test 1: Call host with no SSL
			 */
			URL u = new URL("http://localhost:8080/");
//			URL u = new URL("https://localhost:8443/asmusp/rest/periodos");
//			URL u = new URL("http://localhost:8080/asmusp/rest/periodos");
			
			/**
				Test 2: Call host with SSL, uncomment test.myHostVerifier(); and add VM arguments before running class 
			 */
//			URL u = new URL("https://localhost:8443/");

			/**
				Test 3: Call public known host with no SSL 
			 */
//			URL u = new URL("http://mail.google.com/mail");
			

			/**
				Test 4: Call public known host with SSL, no need for test.myHostVerifier() and VM arguments 
			 */
//			URL u = new URL("https://mail.google.com/mail");
			
			HttpURLConnection uc = (HttpURLConnection) u.openConnection();
			uc.setRequestProperty("chave_acesso", "C7639JFYhh9qv41E8vJ43I2");
			InputStreamReader isr = new InputStreamReader(uc.getInputStream());
			BufferedReader br = new BufferedReader(isr);
			String tmp = "";
			StringBuffer response = new StringBuffer();
			while((tmp = br.readLine()) != null){
				response.append(tmp + "\n");
			}
			br.close();
			System.out.println(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}

}
