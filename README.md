1) Create a Certificate:
    keytool -genkeypair -alias MyCert -keyalg RSA -keystore "F:\MyCert.cert"

2) Config tomcat to use SSL:
    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol" SSLEnabled="true"
           maxThreads="150" scheme="https" secure="true" clientAuth="false" sslProtocol="TLS" 
           keystoreFile="F:\MyCert.cert" keystorePass="password"/>

Info: Access via browser should be working with non-signed certificate.

3) Open class Test.class

4) Config VM arguments passing mock certification data (MyCert.cert)
    -Djavax.net.ssl.trustStore=F:/MyCert.cert
    -Djavax.net.ssl.trustStorePass=mustard

5) Run class over HTTP and HTTP protocols to test it.